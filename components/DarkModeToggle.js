import React from 'react'
import { COLORS } from '../styles/theme/colors'

import styles from './DarkModeToggle.module.css'
export const ThemeContext = React.createContext();
export const ThemeProvider = ({children}) => {
  const [colorMode, rawSetColorMode] = React.useState(undefined);
  React.useEffect(() => {
    const root = window.document.documentElement;
    const initialColorValue = root.style.getPropertyValue(
      '--initial-color-mode'
    );
    rawSetColorMode(initialColorValue);
  }, []);

  const setColorMode = (value) => {
    const root = window.document.documentElement;
    rawSetColorMode(value);
    localStorage.setItem('color-mode', value);
    Object.entries(COLORS).forEach(([name, colorTheme]) => {
        const cssVarName = `--${name}`
        root.style.setProperty(cssVarName, colorTheme[value])
    });
  };

  const toggleColor = () => {
    if (typeof(colorMode === 'string'))
      setColorMode(colorMode === 'light' ? 'dark' : 'light')
  }

  return (
    <ThemeContext.Provider value={{ colorMode, toggleColor }}>
      {children}
    </ThemeContext.Provider>
  )

}


const DarkModeToggle = ({hasLabel}) => {
  const { colorMode, toggleColor } = React.useContext(ThemeContext)

  if (hasLabel){
    return (
      <label className={styles.text_label}>
        <span className="align-top">
          {colorMode === 'light' ? 'Dark Mode' : 'Light Mode'}
        </span>
        <input id="toggle" checked={colorMode === 'light'} onChange={toggleColor} style={{ visibility: (colorMode === undefined ? 'hidden' : 'visible')}}
          className={"ms-3 align-text-bottom " + styles.toggle} type="checkbox" />
      </label>
    )
  }
  return (
    <label className={styles.label}>
      <input id="toggle" checked={colorMode === 'light'} onChange={toggleColor} style={{ visibility: (colorMode === undefined ? 'hidden' : 'visible')}}
      className={styles.toggle} type="checkbox" />
    </label>
  )

}
export default DarkModeToggle