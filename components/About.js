import React from 'react';
import styles from './About.module.css'

const About = ({ about_text }) => {
    return (
        <div className="container-fluid px-0">
            <div className="row">
                <div className="col d-flex justify-content-center">
                    <div className={"card " + styles.card}>
                        <div className="row g-0">
                            <div className="col-md-9">
                                <div className="card-body">
                                    <h5 className={"card-title " + styles.card_title}>About me</h5>
                                    <p className={"card-text " + styles.card_text}>
                                        <span dangerouslySetInnerHTML={{ __html: about_text}}></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default About