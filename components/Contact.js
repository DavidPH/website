import React, { useState } from 'react';
import styles from './Contact.module.css'

const Contact = () => {
    const [errorVisible, setErrorVisible] = useState(false);
    const [successVisible, setSuccessVisible] = useState(false);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");
    const [showSpinner, setSpinner] = useState(false);

    const resetForm = () => {
        [setName, setEmail, setMessage].forEach(f => f(""))
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        setSuccessVisible(false);
        setErrorVisible(false);
        var data = {
            name: name,
            email: email,
            message: message
        }
        setSpinner(true);
        fetch('/api/contact', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((res) => {
            console.log("Response received")
            setSpinner(false)
            if (res.status === 200){
                setSuccessVisible(true);
                resetForm();
            }
            else {
                setErrorVisible(true);
            }
        })
    }


    return (
        <div className="container-fluid px-0 mt-2" style={{fontFamily: "Segoe UI"}}>
            <div className="row">
                <div className="col d-flex justify-content-center">
                    <div className={"card bg-transparent " + styles.card } >
                        <div className="card bg-transparent card-outline-secondary">
                            <div className={"card-header " + styles.header}>
                                    <h3 style={{fontSize: "1.75rem", color:"#343a40"}} className="card-title my-1 text-truncate">
                                        <a href="mailto:david@dphennessy.com" className={"text-decoration-none " + styles.header_text}>
                                                David@dphennessy.com
                                        </a>
                                    </h3>
                            </div>
                            <div className={"card-body pb-3 " + styles.body}>
                                <form className="form" autoComplete="off" onSubmit={(e) => handleSubmit(e)}>
                                    <fieldset>
                                        <div className="alert alert-danger pt-1 pb-0" hidden={!errorVisible}>
                                            <strong>Error!</strong>
                                            <p className="mb-1">Couldn't send email.</p>
                                        </div>
                                        <div className="alert alert-success pt-1 pb-0" hidden={!successVisible}>
                                            <strong>Email sent!</strong>
                                            <p className="mb-1">I'll try to get back to you as soon as possible.</p>
                                        </div>

                                        <label className="mb-0">Name</label>
                                        <div className="row mb-1">
                                            <div className="col-lg-12">
                                                <input className="form-control" type="text" value={name} onChange={e => setName(e.target.value)} required />
                                            </div>
                                        </div>
                                        <label className="mb-0">Email</label>
                                        <div className="row mb-1">
                                            <div className="col-lg-12">
                                                <input className="form-control" type="email" value={email} onChange={e => setEmail(e.target.value)} required />
                                            </div>
                                        </div>
                                        <label className="mb-0">Message</label>
                                        <div className="row mb-1">
                                            <div className="col-lg-12">
                                                <textarea rows="6" value={message} onChange={e => setMessage(e.target.value)} className="form-control" required />
                                            </div>
                                        </div>
                                        <div className="spinner-border text-secondary mt-2 py-1 float-end" hidden={!showSpinner}>
                                            <span className="visually-hidden">Loading...</span>
                                        </div>
                                        <button type="submit" hidden={showSpinner} className="btn btn-lg btn-secondary mt-2 py-1 float-end">
                                            Send
                                        </button>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Contact