import React from 'react';
import styles from './Menu.module.css'
import DarkModeToggle from './DarkModeToggle'
import { ThemeProvider } from './DarkModeToggle'


const Menu = ({ projects, refs }) => {
    const nav_collapse_ref = React.createRef();
    const goToRef = (ref) => {
        var r = nav_collapse_ref.current
        var time = r.classList.contains("show") ? 400 : 0
        r.classList.remove("show")
        setTimeout(() => {
            ref.current.scrollIntoView({
                behavior: 'smooth',
                block: 'start',
            });
    }, time)
    }

    return (
        <nav className={"navbar navbar-expand-lg pt-3 " + styles.soft_font}>
            <div className={"container-fluid"}>
                <a className={"navbar-brand "} onClick={() => goToRef(refs[0])} style={{ cursor: "pointer" }}>
                    <img src="/images/dp_logo_transparent.png" height="55" className={"d-inline-block " + styles.logo} alt="logo" />
                    <span className={"ms-3 me-lg-3 ms-3 me-md-3 " + styles.nav_title}>
                        David
                        <span className="d-none d-sm-inline"> Patrick Hennessy </span>
                        <span className="d-inline d-sm-none"> P. H. </span>
                    </span>
                </a>
                <button className={"navbar-toggler "+ styles.toggler_btn} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown">
                    <span className={"navbar-toggler-icon " + styles.toggler_icon}/>
                </button>
                <ThemeProvider>
                <div ref={nav_collapse_ref} className="collapse navbar-collapse " id="navbarNavDropdown">
                    <ul className="navbar-nav fw-bolder">
                        <li className="nav-item pe-lg-3 pe-sm-0 text-nowrap" style={{ cursor: "pointer"}}>
                            <a className={"nav-link " + styles.soft_font} onClick={() => goToRef(refs[0])}>About me</a>
                        </li>
                        <li className="nav-item pe-lg-3 pe-sm-0" style={{ cursor: "pointer" }}>
                            <a className={"nav-link " + styles.soft_font} onClick={() => goToRef(refs[2])}>Email</a>
                        </li>
                        <li className="nav-item pe-lg-3 pe-sm-0">
                            <a className={"nav-link " + styles.soft_font} href="https://gitlab.com/DavidPH/" target="_blank">Gitlab</a>
                        </li>                       
                        <li className="nav-item dropdown">
                            <a className={"nav-link dropdown-toggle " + styles.soft_font} id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown">
                                Projects
                            </a>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                {
                                    projects.map((project, i) => {
                                        return (
                                            <li key={"project_menu_item"+ i}>
                                                <a className={"dropdown-item " + styles.dropdown} onClick={() => goToRef(refs[i + 3])}>
                                                    {project.name}
                                                </a>
                                            </li>
                                    )})
                                }
                            </ul>
                        </li>
                        <li className="nav-item d-inline d-lg-none">
                            <div>
                                <DarkModeToggle hasLabel={true}/>
                            </div>
                        </li>
                    </ul>
                    <div className={"ms-2 d-none d-lg-inline"}>
                        <DarkModeToggle/>
                    </div>
                </div>
                </ThemeProvider>
            </div>
        </nav>
    )

}
export default Menu