import React, { useState } from 'react';
import styles from './ProjectCard.module.css';

const ProjectCard = ({ project, proj_num }) => {
    
    if (!project){
        return (
            <div className="container-fluid px-0 mt-4">
                <div className="d-flex justify-content-center">
                    <div className={"card " + styles.proj_card}>
                        <div className="card-body d-flex justify-content-center">
                            <div className="spinner-border text-secondary me-3 align-bottom"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    
    return (
        <div className="container-fluid px-0 mt-4">
            <div className="d-flex justify-content-center">
                <div className={"card " + styles.proj_card}>
                    <div className="row g-0">
                        <div className={"col-md-3 "}>
                            <img data-bs-toggle="modal" data-bs-target={"#projModal" + proj_num }
                                className={"card-img img-thumbnail border-3 ms-md-1 my-md-1 " + styles.proj_img}
                                src={project.image}/>
                        </div>
                        <div className="col-md-8">
                            <div className="card-body">
                                    <h5 className="card-title">
                                        <a className={"text-decoration-none " + styles.proj_title} href={project.link} target="_blank" rel="noopener noreferrer">
                                            {project.name}
                                        </a>
                                    </h5>
                                <div className={"card-text mb-5 " + styles.proj_text}>
                                    <div dangerouslySetInnerHTML={{ __html: project.description}}></div>
                                </div>
                                <p className={"my-4 card-text " + styles.proj_link}>
                                    <a className="text-decoration-none" href={project.link} target="_blank" rel="noopener noreferrer">
                                        <small className={"ml-2 "}>View Project</small>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            { /* Modal displaying fullscreen project image when it's clicked */ }
            <div className="modal fade" id={"projModal" + proj_num} tabIndex="-1">
                <div className="modal-xl modal-dialog">
                    <div className="container">
                        <div className="row g-0 d-flex justify-content-center">
                            <img className={styles.proj_img_modal} src={project.image}/>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}
export default ProjectCard