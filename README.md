## www.dphennessy.com

This is a website made for personal use only; to display some projects, and other miscellaneous things.  
The data displayed on the front page is stored under `projects/data.json`, currently only a project list and the about section. Depending on future usage I might transfer this into an actual database but for now this is more than enough.

## API

Currently only supports GET requests, but plan is to implement a way I can edit the aforementioned `data.json` using POST requests instead of doing it manually.

```bash
GET /api/projects 
GET /api/projects/[project_id]
```

## TODO

- [x] Swapping from react-strap & bootstrap 4 to just Bootstrap 5
- [x] Dark Mode toggle
- [ ] Login system to edit page information.
- [ ] More API routes


