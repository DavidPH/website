import { COLORS } from './colors'

export function setColorsByTheme() {
    const colors = '⚡';
    
    function getThemeColor(){
        const colorSetting = window.localStorage.getItem('color-mode');
        if (typeof(colorSetting) === "string") {
            return colorSetting;
        }

        const mql = window.matchMedia('(prefers-color-scheme: dark)').matches;
        if (typeof(mql) === "boolean") {
            return mql ? 'dark' : 'light';
        }
        return 'light';
        
    }

    const colorMode = getThemeColor()
    const root = document.documentElement;
    Object.entries(colors).forEach(([name, colorTheme]) => {
        const cssVarName = `--${name}`
        root.style.setProperty(cssVarName, colorTheme[colorMode])
    })
    root.style.setProperty("--initial-color-mode", colorMode)
}

export function FallbackStyles() {
    const cssVariableString = Object.entries(COLORS).reduce(
      (acc, [name, colorTheme]) => {
        return `${acc}\n--${name}: ${colorTheme.light};`
      },
      ''
    )
    const wrappedInSelector = `html { ${cssVariableString} }`
    return <style>{wrappedInSelector}</style>
  }

export function MagicScriptTag() {
    const boundFn = String(setColorsByTheme).replace("'⚡'", JSON.stringify(COLORS));
    let calledFunction = `(${boundFn})()`
    return <script dangerouslySetInnerHTML={{ __html: calledFunction }}/>
}