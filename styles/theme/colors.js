export const COLORS = {
    'dp-background': {
        light: '#F8F9FA',
        dark: '#121212',
    },
    'dp-foreground': {
        light: '#FFFFFF',
        dark: 'rgba(255, 255, 255, 0.1)'
    },
    'dp-foreground-shadow': {
        light: '0px 0px 5px 5px rgba(0,0,0,0.2)',
        dark: '0px 0px 5px 5px rgba(178,178,178,0.2)'
    },
    'dp-section-divider': {
        light: 'rgba(0, 0, 0, .1)',
        dark: 'rgba(255, 255, 255, 1)'
    },
    'dp-menu-title':{
        light: 'rgba(0, 0, 0, .7)',
        dark: 'rgba(255, 255, 255, .7)'
    },
    'dp-menu-color': {
        light: '#FFFFFF',
        dark: '#393939'
    },
    'dp-menu-item-color': {
        light: 'rgba(0, 0, 0, 0.55)',
        dark: 'rgba(255, 255, 255, 0.55)'
    },
    'dp-menu-divider': {
        light: '#a5a5a5',
        dark: 'rgba(255, 255, 255, .55)'
    },
    'dp-logo': {
        light: '',
        // https://codepen.io/sosuke/pen/Pjoqqp
        dark: 'invert(76%) sepia(44%) saturate(9%) hue-rotate(209deg) brightness(99%) contrast(92%)' // #C9CBCE
        // dark: 'invert(100%) sepia(69%) saturate(655%) hue-rotate(179deg) brightness(103%) contrast(96%)'
    },
    'dp-card-background': {
        light: '#F8F9FA',
        dark: '#393939'
    },
    'dp-title-color':{
        light: '#000000',
        dark: '#FFFFFF',
    },
    'dp-text-color': {
        light: '#6C757D',
        dark: '#C9CBCE'
    },
    'dp-text-color-black': {
        light: '#000000',
        dark: '#C9CBCE'
    },
    'dp-proj-link': {
        light: 'rgba(0, 0, 0, 0.5)',
        dark: 'rgba(255, 255, 255, 0.5)'
    },
    'dp-card-border': {
        light: 'rgb(222, 226, 230)',
        dark: 'rgb(79, 79, 79)'
    },
    'dp-code-background': {
        light: '#eee',
        dark: 'rgb(79, 79, 79)'
    }
    
}