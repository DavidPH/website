import 'bootstrap/dist/css/bootstrap.css';
import '../styles/global.css'
import { useEffect } from 'react'
import Head from 'next/head'
export default function App({ Component, pageProps }) {
    useEffect(() => {
        typeof document !== undefined ? require('bootstrap/dist/js/bootstrap') : null
    }, [])
    return (
        <>
            <Head>
                {/* favicons */}
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
                <link rel="manifest" href="/site.webmanifest"/>
                <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5"/>
                <meta name="msapplication-TileColor" content="#00aba9"/>
                <meta name="theme-color" content="#ffffff"/>

                <title>David PH</title>
                <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
                <meta name="description" content="See a list of my projects, or contact me at david@dphennessy.com"/>
                <meta name="robots" content="index, nofollow"/>
                <link rel="canonical" href="https://dphennessy.com"/>

            </Head>
            <Component {...pageProps} />
        </>
    )
}