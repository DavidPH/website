import React from 'react';
import Menu from '../components/Menu';
import About from '../components/About';
import ProjectCard from '../components/ProjectCard';
import Contact from '../components/Contact';
import styles from '../styles/main_page.module.css';
import path from 'path'
import { promises as fs } from 'fs'

function Main({ proj_data, about_data }) {
  let refs = Array.from({length: 3}, () => React.createRef());
  if (proj_data){
    refs = refs.concat(Array.from({ length: proj_data.length }, () => React.createRef()));
  }
  return (
      <div className={styles.background}>
        <div className={"col-md-10 offset-md-1 " + styles.foreground} >
          { /* Sticky Navbar */ }
          <div className="container-fluid sticky-top">
            <div className="row">
              <div className={"col d-flex justify-content-center " + styles.menu } >
                <Menu refs={refs} projects={proj_data}/>
              </div>
            </div>
          </div>

          { /* Content Body */ }
          <div className="container-fluid mt-3">
            <noscript>
              <div className="row">
                <div className="col d-flex justify-content-center ">
                  <div className="alert alert-danger pt-1 pb-0" style={{width: '80%'}}>
                    <strong>Javascript Disabled</strong>
                    <p className="mb-1">Some features won't work properly.</p>
                  </div>
                </div>
              </div>
            </noscript>
            { /* About */ }
            <div className="row">
                <span ref={refs[0]} style={{ scrollMarginTop: '110px' }} />
                <div className={"col"}>
                  <About about_text={about_data}/>
                </div>
            </div>
            <div className="row">
                <div className={"col d-flex justify-content-center "} >
                  <hr className={"mt-4 "+ styles.divider}/>
                </div>
            </div>
            
            { /* Projects */ }
            <div className="row">
                <span ref={refs[1]} style={{ scrollMarginTop: '70px' }} />
                <div className={"col"} >
                  <div className="container-fluid px-0 py-0 my-0 mx-0">
                    {
                      proj_data.map((project, i) => 
                      {
                        return (
                            <span key={"proj_span" + (i+3)} ref={refs[i + 3]} style={{ scrollMarginTop: '115px' }} >
                              <ProjectCard project={project} proj_num={i}/>
                            </span>
                        )})
                    }
                  </div>
                </div>
            </div>
            <div className="row">
                <div className={"col d-flex justify-content-center "} >
                  <hr className={"my-4 "+ styles.divider}/>
                </div>
            </div>

            { /* Contact */ }
            <div className="row pb-4">
                <span ref={refs[2]} style={{ scrollMarginTop: '85px' }} />
                <div className={"col d-flex justify-content-center "} >
                  <Contact/>
                </div>
            </div>
            
          </div>
        </div>
      </div>
    );
}

export async function getStaticProps() {
  const data_path = path.join(process.cwd(), 'projects/data.json')
  const file_data = await fs.readFile(data_path)
  const data = JSON.parse(file_data)
  return {
    props: {
      proj_data: data.projects, 
      about_data: data.about
    },
    revalidate: 10,
  }
}

export default Main