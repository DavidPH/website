export default function (req, res) {
    let nodemailer = require('nodemailer')
    const USER = process.env.USER;
    const PASS = process.env.PASS;
    const transporter = nodemailer.createTransport({
      port: 465,
      host: "mail.dphennessy.com",
      auth: {
        user: USER,
        pass: PASS,
      },
      secure: true,
    })
    const mailData = {
      from: USER,
      to: 'david@dphennessy.com',
      subject: `Message From ${req.body.name}`,
      text: req.body.message + " | Email from: " + req.body.email,
      html: `<div>${req.body.message}</div><p>Sent from: ${req.body.email}</p>`
    }
    transporter.sendMail(mailData, function (err, info) {
      if(err){
          console.log(err)
          res.status(500).json()
      }
      else{
          res.status(200).json()
      }
    })
  }