const fsp = require('fs').promises;
import path from 'path';
export default async function (req, res) {
    try {
        const { method } = req
        const data_path = path.join(process.cwd(), 'projects/data.json')
        switch (method) {
            case 'GET':
                const file_data = await fsp.readFile(data_path)
                const data = JSON.parse(file_data)
                res.status(200).json(data.projects)
            break
        case 'POST':
                // TODO
                res.status(500).json()
            break
        default:
            res.setHeader('Allow', ['GET', 'POST'])
            res.status(405).json(`Method ${method} Not Allowed`)
        }
    }catch (e){
        console.log(e);
        res.status(500).send('Error')
    }
}