const fsp = require('fs').promises;
import path from 'path';
export default async function ({ method, query: {id} }, res){
    try {
        const data_path = path.join(process.cwd(), 'projects/data.json')
        switch (method) {
            case 'GET':
                const file_data = await fsp.readFile(data_path)
                const data = JSON.parse(file_data)
                if (id == "about") {
                    res.status(200).json({about: data.about})
                }
                else if (id == "placeholder"){
                    res.status(200).json(data.placeholder_project)
                }
                else if (id < data.projects.length){
                    res.status(200).json(data.projects[id])
                }
                else{
                    res.status(404).json({message: `Project with id: ${id} not found.`})
                }
            break
        case 'POST':
                // TODO
                res.status(500).send('Error')
            break
        default:
            res.setHeader('Allow', ['GET', 'POST'])
            res.status(405).json(`Method ${method} Not Allowed`)
        }
    }catch (e){
        console.log(e);
        res.status(500).send("Error.")
    }
}